# Étape de préparation pour installer les dépendances
FROM node:lts-alpine as prep

WORKDIR /app

# Copie le package.json et le package-lock.json pour installer les dépendances
COPY package*.json ./

# Installe les dépendances en mode production
RUN npm install --only=production

# Copie le reste des fichiers du projet
COPY . .

# Étape de build, utilise l'image de node lts-alpine comme base
FROM node:lts-alpine as build

# Définit le mode production pour l'environnement Node.js
ENV NODE_ENV production

# Définit le répertoire de travail et l'utilisateur
WORKDIR /app

USER node

# Copie les dépendances installées depuis l'étape de préparation
COPY --from=prep /app /app

# Expose le port sur lequel votre application va s'exécuter
EXPOSE 3000

# Définit la commande pour démarrer votre application
CMD ["npm", "run", "start"]
